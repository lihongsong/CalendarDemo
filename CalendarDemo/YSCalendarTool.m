//
//  YSCalendarTool.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/20.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSCalendarTool.h"
#import "YSIndexPath.h"
#import "YSCalendarConfig.h"

@implementation YSCalendarTool

+ (NSInteger)getNowWeekday {
    
    return [YSCalendarTool getWeekdayWithDate:[NSDate date]];
}

+ (NSInteger)getWeekdayWithDate:(NSDate *)date
{
    NSCalendar *calendar = [YSCalendarTool getCalendar];
    
    NSDateComponents *components = [YSCalendarTool getDateComponents:date Calendar:calendar];
    
    return [components weekday] - 1;
}

+ (NSInteger)getMonthFirstDayWeekdayWithDate:(NSDate *)date
{
    NSCalendar *calendar = [YSCalendarTool getCalendar];
    
    NSDateComponents *components = [YSCalendarTool getDateComponents:date Calendar:calendar];
    
    [components setDay:1];

    NSDate *firstDay = [calendar dateFromComponents:components];
    
    return [YSCalendarTool getWeekdayWithDate:firstDay];
}

+ (NSInteger)getMonthDaysWithDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSRange days = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    
    return days.length;
}

+ (NSDateComponents *)getDateComponents:(NSDate *)date Calendar:(NSCalendar *)calendar
{
    NSCalendarUnit unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    
    return components;
}

+ (NSCalendar *)getCalendar
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    calendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    
    return calendar;
}

+ (NSInteger)getYearWithDate:(NSDate *)date
{
    return [[YSCalendarTool getDateComponents:date Calendar:[YSCalendarTool getCalendar]] year];
}

+ (NSInteger)getMonthWithDate:(NSDate *)date
{
    return [[YSCalendarTool getDateComponents:date Calendar:[YSCalendarTool getCalendar]] month];
}

+ (YSIndexPath *)indexPathWithPoint:(CGPoint)point Rect:(CGRect)rect
{
    NSInteger list = point.x / CGRectGetWidth(rect) * TOTALLIST;
    NSInteger row = point.y / CGRectGetWidth(rect) * TOTALROW;
    
    YSIndexPath *indexPath = [YSIndexPath indexPathWithList:list Row:row];
    
    return indexPath;
}

@end
