//
//  YSIndexPath.h
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/27.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YSIndexPath : NSObject

@property (assign, nonatomic)  NSInteger row;
@property (assign, nonatomic)  NSInteger list;

+ (instancetype)indexPathWithList:(NSInteger)list Row:(NSInteger)row;

@end
