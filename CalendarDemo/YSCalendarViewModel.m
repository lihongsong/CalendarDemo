//
//  YSCalendarViewModel.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSCalendarViewModel.h"
#import "YSCalendarTool.h"

@interface YSCalendarViewModel()

@property (strong, nonatomic) NSMutableArray *data;

@end

@implementation YSCalendarViewModel

- (instancetype)init
{
    if(self = [super init]){
        self.todayIndex = 0;
        
        NSInteger month = [YSCalendarTool getMonthWithDate:[NSDate date]];
        NSInteger year = [YSCalendarTool getYearWithDate:[NSDate date]];
        
        [self YSCalendarSetMonth:month Year:year];
    }
    return self;
}

#pragma mark SET 
- (void)setMonth:(NSInteger)month
{
    if(month == 0){
        _month = 12;
        _year--;
    }else if (month == 13){
        _month = 1;
        _year++;
    }else{
        _month = month;
    }
    
    [self refreshData];
}

- (void)setYear:(NSInteger)year
{
    _year = year;
    [self refreshData];
}

- (void)YSCalendarSetMonth:(NSInteger)month Year:(NSInteger)year
{
    self.month = month;
    self.year = year;
    [self refreshData];
}

- (void)refreshData{
    
    NSDateComponents *components = [NSDateComponents new];
    [components setMonth:self.month];
    [components setYear:self.year];
    [components setDay:1];
    
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    NSInteger firstDay = [YSCalendarTool getMonthFirstDayWeekdayWithDate:date] - 1;
    
    NSInteger days = [YSCalendarTool getMonthDaysWithDate:date];
    
    NSMutableArray * array = [NSMutableArray array];
    
    for (int i = 0 ; i < days + firstDay; i++) {
        
        if(i < firstDay){
            [array addObject:[NSString stringWithFormat:@"%d",0]];
        }else{
            [array addObject:[NSString stringWithFormat:@"%d",i - (int)firstDay]];
        }
    }
    
    self.data = [array copy];
    
    if([self.delegate respondsToSelector:@selector(YSCalendarViewModelDidChangeDataArray:)]){
        [self.delegate YSCalendarViewModelDidChangeDataArray:self.dataArray];
    }
}

- (NSMutableArray *)dataArray
{
    return self.data;
}



@end
