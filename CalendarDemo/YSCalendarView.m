//
//  YSCalendarView.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSCalendarView.h"
#import "YSCalendarCollectionView.h"
#import "YSCalendarHeaderView.h"
#import "YSCalendarViewModel.h"
#import "YSCalendarSelectView.h"
#import "YSCalendarCollectionViewCell.h"

#define HEADERHEIGHT 100

#define SELFWIDTH CGRectGetWidth(self.frame)
#define SELFHEIGHT CGRectGetHeight(self.frame)

@interface YSCalendarView()
<UICollectionViewDelegate,
UICollectionViewDataSource,
YSCalendarViewModelDelegate,
YSCalendarHeaderViewDelegate,
YSCalendarSelectViewDelegate>

@property (strong, nonatomic) YSCalendarCollectionView *calendarView;

@property (strong, nonatomic) YSCalendarHeaderView *headerView;

@property (strong, nonatomic) YSCalendarViewModel *viewModel;

@property (strong, nonatomic) YSCalendarSelectView *selectedView;

@end

@implementation YSCalendarView

- (instancetype)init{
    if(self = [super init]){
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame]){
        
        self.viewModel = [YSCalendarViewModel new];
        self.viewModel.delegate = self;
        
        [self createHeaderView:frame];
        [self createCalendarView:frame];
        [self createSelectedView:frame];
        
        self.headerView.backgroundColor = [UIColor redColor];
        self.calendarView.backgroundColor = [UIColor redColor];
    }
    return self;
}

#pragma mark - UI 
- (void)createHeaderView:(CGRect)rect
{
    CGRect headerRect = CGRectMake(0, 0, SELFWIDTH, HEADERHEIGHT);
    self.headerView = [[YSCalendarHeaderView alloc] initWithFrame:headerRect];
    [self addSubview:self.headerView];
    self.headerView.delegate = self;
    self.headerView.month = self.viewModel.month;
    self.headerView.year = self.viewModel.year;
}

- (void)createCalendarView:(CGRect)rect
{
    CGRect calendarRect = CGRectMake(0, HEADERHEIGHT, SELFWIDTH, SELFHEIGHT - HEADERHEIGHT);
    self.calendarView = [[YSCalendarCollectionView alloc] initWithFrame:calendarRect];
    [self addSubview:self.calendarView];
    self.calendarView.delegate = self;
    self.calendarView.dataSource = self;
}

- (void)createSelectedView:(CGRect)rect
{
    CGRect selectedRect = CGRectMake(0, HEADERHEIGHT, SELFWIDTH, SELFHEIGHT - HEADERHEIGHT);
    self.selectedView = [[YSCalendarSelectView alloc] initWithFrame:selectedRect];
    [self addSubview:self.selectedView];
    self.selectedView.delegate = self;
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.viewModel.dataArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    YSCalendarCollectionViewCell *cell = (YSCalendarCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellID
                                                                                                                   forIndexPath:indexPath];
    if([self.viewModel.dataArray[indexPath.row] integerValue] == 0){
        cell.dayLabel.text = @"";
    }else{
        cell.dayLabel.text = self.viewModel.dataArray[indexPath.row];
    }
    
    return cell;
}

#pragma mark - YSCalendarHeaderViewDelegate
- (void)YSCalendarLeftButtonClick:(UIButton *)sender{
    self.viewModel.month--;
    self.headerView.month = self.viewModel.month;
    self.headerView.year = self.viewModel.year;
}

- (void)YSCalendarRightButtonClick:(UIButton *)sender
{
    self.viewModel.month++;
    self.headerView.month = self.viewModel.month;
    self.headerView.year = self.viewModel.year;
}

#pragma mark - YSCalendarViewModelDelegate
- (void)YSCalendarViewModelDidChangeDataArray:(NSMutableArray *)dataArray
{
    [self.calendarView reloadData];
}

#pragma mark - YSCalendarSelectedViewDelegate
- (void)YSCalendarSelectViewDidSelectEnd:(NSRange)range
{
    
}

@end
