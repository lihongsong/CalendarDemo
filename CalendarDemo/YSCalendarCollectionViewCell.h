//
//  YSCalendarCollectionViewCell.h
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const cellID = @"YSCalendarCollectionViewCell";

@interface YSCalendarCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;

@end
