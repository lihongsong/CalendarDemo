//
//  YSCalendarTool.h
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/20.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YSIndexPath;

@interface YSCalendarTool : NSObject

+ (NSInteger)getNowWeekday;

+ (NSInteger)getWeekdayWithDate:(NSDate *)date;

+ (NSInteger)getMonthFirstDayWeekdayWithDate:(NSDate *)date;

+ (NSInteger)getMonthDaysWithDate:(NSDate *)date;

+ (NSInteger)getYearWithDate:(NSDate *)date;

+ (NSInteger)getMonthWithDate:(NSDate *)date;

/*!
 *  transfer
 */

+ (YSIndexPath *)indexPathWithPoint:(CGPoint)point Rect:(CGRect)rect;

@end
