//
//  YSIndexPath.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/27.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSIndexPath.h"

@implementation YSIndexPath

+ (instancetype)indexPathWithList:(NSInteger)list Row:(NSInteger)row
{
    YSIndexPath *indexPath = [[YSIndexPath alloc] init];
    indexPath.row = row;
    indexPath.list = list;
    return indexPath;
}

@end
