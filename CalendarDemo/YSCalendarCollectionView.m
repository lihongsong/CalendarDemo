//
//  YSCalendarCollectionView.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSCalendarCollectionView.h"
#import "YSCalendarFlowLayout.h"
#import "YSCalendarCollectionViewCell.h"

@implementation YSCalendarCollectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    YSCalendarFlowLayout *flowLayout = [YSCalendarFlowLayout YSCalendarFlowLayoutWithRect:frame];
    
    if(self == [super initWithFrame:frame collectionViewLayout:flowLayout]){
        
        self.userInteractionEnabled = NO;
        NSString *nibName = NSStringFromClass([YSCalendarCollectionViewCell class]);
        [self registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellWithReuseIdentifier:cellID];
    }
    return self;
}



@end
