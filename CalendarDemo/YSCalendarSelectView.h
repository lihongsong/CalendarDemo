//
//  YSCalendarSelectView.h
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/20.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YSIndexPath;

@protocol YSCalendarSelectViewDelegate <NSObject>

- (void)YSCalendarSelectViewDidSelectEnd:(NSRange)range;

@end

@interface YSCalendarSelectView : UIView

@property (strong, nonatomic) UIColor *lineColor;

@property (assign, nonatomic) CGFloat lineWidth;

@property (weak, nonatomic) id<YSCalendarSelectViewDelegate>delegate;

@property (strong, nonatomic) YSIndexPath *limitStart;

@property (strong, nonatomic) YSIndexPath *limitEnd;

@end
