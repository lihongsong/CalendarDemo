//
//  ViewController.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "ViewController.h"
#import "YSCalendarView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    YSCalendarView *calendar = [[YSCalendarView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 500)];
    [self.view addSubview:calendar];
    
    // Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
