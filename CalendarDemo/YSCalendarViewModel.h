//
//  YSCalendarViewModel.h
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol YSCalendarViewModelDelegate <NSObject>

- (void)YSCalendarViewModelDidChangeDataArray:(NSMutableArray *)dataArray;

@end

@interface YSCalendarViewModel : NSObject

@property (strong, readonly,nonatomic) NSMutableArray *dataArray;

@property (assign, nonatomic) NSInteger *todayIndex;

@property (weak, nonatomic) id<YSCalendarViewModelDelegate>delegate;

- (void)YSCalendarSetMonth:(NSInteger)month Year:(NSInteger)year;

@property (assign, nonatomic) NSInteger month;

@property (assign, nonatomic) NSInteger year;

@end
