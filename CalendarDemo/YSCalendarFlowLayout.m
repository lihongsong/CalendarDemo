
//
//  YSCalendarFlowLayout.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSCalendarFlowLayout.h"
#import "YSCalendarConfig.h"

@implementation YSCalendarFlowLayout

+ (instancetype)YSCalendarFlowLayoutWithRect:(CGRect)rect
{
    CGFloat width = CGRectGetWidth(rect);
    CGFloat height = CGRectGetHeight(rect);
    
    CGFloat perWidth = width / TOTALLIST;
    CGFloat perheight = height / TOTALROW;
    
    YSCalendarFlowLayout *layout = [[YSCalendarFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(perWidth, perheight);
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    return layout;
}

@end
