//
//  YSCalendarSelectView.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/20.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSCalendarSelectView.h"
#import "YSCalendarConfig.h"
#import "YSIndexPath.h"
#import "YSCalendarTool.h"

#define PERROWHEIGHT CGRectGetHeight(self.frame) / TOTALROW

#define PERLISTWIDTH CGRectGetWidth(self.frame) / TOTALLIST

@interface YSCalendarSelectView()

@property (strong, nonatomic) YSIndexPath *startIndexPath;

@property (strong, nonatomic) YSIndexPath *currentIndexPath;

@end

@implementation YSCalendarSelectView

- (instancetype)init{
    if(self = [super initWithFrame:CGRectZero]){
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame]){
        self.lineColor = [[UIColor yellowColor] colorWithAlphaComponent:0.3f];
        self.lineWidth = 15.0f;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


#pragma mark - TouchEvent
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:touch.view];
    
    self.startIndexPath = [YSCalendarTool indexPathWithPoint:touchPoint Rect:self.frame];
    
    self.currentIndexPath = self.startIndexPath;
    
    [self setNeedsDisplay];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:touch.view];
    
    self.startIndexPath = [YSCalendarTool indexPathWithPoint:touchPoint Rect:self.frame];
    
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesMoved:touches withEvent:event];
}

- (void)drawRect:(CGRect)rect
{
    YSIndexPath *startIndexpath;
    YSIndexPath *endIndexpath;
    
    if(self.startIndexPath == nil || self.currentIndexPath == nil){
        return;
    }else{
        if(_startIndexPath.row <= _currentIndexPath.row){
            startIndexpath = _startIndexPath;
            endIndexpath    = _currentIndexPath;
        }else{
            startIndexpath = _currentIndexPath;
            endIndexpath = _startIndexPath;
        }
        
        for (NSInteger i = startIndexpath.row; i <= endIndexpath.row; i++) {
            
            if(i == startIndexpath.row && i == endIndexpath.row){
                [self drawLineFrom:startIndexpath ToIndexPath:endIndexpath];
            }else if(i == startIndexpath.row && i <  endIndexpath.row){
                [self drawLineFromIndexPathToEnd:startIndexpath];
            }else if(i >  startIndexpath.row && i <  endIndexpath.row){
                [self drawFullRow:i];
            }else if(i >  startIndexpath.row && i == endIndexpath.row){
                [self drawLineFromStartToIndexPath:endIndexpath];
            }
        }
    }
}

#pragma mark - draw
- (void)drawFullRow:(NSInteger)row
{
    CGFloat startX = PERLISTWIDTH * 0.5;
    CGFloat startY = PERROWHEIGHT * (row + 0.5);
    CGFloat endX = CGRectGetWidth(self.frame) - PERLISTWIDTH * 0.5;
    CGFloat endY = startY;
    
    CGPoint startPoint = CGPointMake(startX, startY);
    CGPoint endPoint = CGPointMake(endX, endY);
    
    [self drawLineFrom:startPoint ToPoint:endPoint];
}

- (void)drawFullList:(NSInteger)list
{
    CGFloat startX = PERLISTWIDTH * (list + 0.5);
    CGFloat startY = PERROWHEIGHT * 0.5;
    
    CGFloat endX = startX;
    CGFloat endY = startY;
    
    CGPoint startPoint = CGPointMake(startX, startY);
    CGPoint endPoint = CGPointMake(endX, endY);
    
    [self drawLineFrom:startPoint ToPoint:endPoint];
}

- (void)drawLineFromIndexPathToEnd:(YSIndexPath *)startIndexPath
{
    YSIndexPath *endIndexPath = [YSIndexPath indexPathWithList:TOTALLIST - 1 Row:startIndexPath.row];
    
    [self drawLineFrom:startIndexPath ToIndexPath:endIndexPath];
}

- (void)drawLineFromStartToIndexPath:(YSIndexPath *)endIndexPath
{
    YSIndexPath *startIndexPath = [YSIndexPath indexPathWithList:0 Row:endIndexPath.row];
    
    CGFloat startX = (startIndexPath.list + 0.5) * PERLISTWIDTH;
    CGFloat startY = (startIndexPath.row + 0.5) * PERROWHEIGHT;
    CGFloat endX = (endIndexPath.list + 0.5) * PERLISTWIDTH;
    CGFloat endY = (endIndexPath.row + 0.5) * PERROWHEIGHT;
    
    CGPoint startPoint = CGPointMake(startX, startY);
    CGPoint endPoint = CGPointMake(endX, endY);
    
    [self drawLineFrom:startPoint ToPoint:endPoint];
}


- (void)drawLineFrom:(CGPoint)startPoint ToPoint:(CGPoint)endPoint
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:startPoint];
    [path addLineToPoint:endPoint];
    [path setLineCapStyle:kCGLineCapRound];
    [path setLineWidth:self.lineWidth];
    [self.lineColor set];
    [path stroke];
}

- (void)drawLineFrom:(YSIndexPath *)startIndexPath ToIndexPath:(YSIndexPath *)endIndexPath
{
    CGFloat startX = (startIndexPath.list + 0.5) * PERLISTWIDTH;
    CGFloat startY = (startIndexPath.row + 0.5) * PERROWHEIGHT;
    CGFloat endX = (endIndexPath.list + 0.5) * PERLISTWIDTH;
    CGFloat endY = (endIndexPath.row + 0.5) * PERROWHEIGHT;
    
    CGPoint startPoint = CGPointMake(startX, startY);
    CGPoint endPoint = CGPointMake(endX, endY);
    
    [self drawLineFrom:startPoint ToPoint:endPoint];
}


@end
