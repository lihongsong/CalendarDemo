//
//  YSCalendarHeaderView.h
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YSCalendarHeaderViewDelegate <NSObject>

- (void)YSCalendarLeftButtonClick:(UIButton *)sender;
- (void)YSCalendarRightButtonClick:(UIButton *)sender;

@end

@interface YSCalendarHeaderView : UIView

@property (weak, nonatomic) id<YSCalendarHeaderViewDelegate>delegate;

@property (assign, nonatomic) NSInteger year;

@property (assign, nonatomic) NSInteger month;

@end
