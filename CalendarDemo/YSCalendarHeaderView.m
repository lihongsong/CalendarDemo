//
//  YSCalendarHeaderView.m
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import "YSCalendarHeaderView.h"

#define BUTTONWIDTH 20
#define BUTTONHEIGHT 30
#define BUTTONSPACING 10

#define WEEKHEIGHT 35
#define TEXTHEIGHT 10
#define TEXTWIDTH 25

#define YEARHEIGHT 20
#define YEARWIDTH 50

#define MONTHHEIGHT 20
#define MONTHWIDTH 100

#define DAYYEARSPACING 20

@interface YSCalendarHeaderView ()

@property (weak, nonatomic) UILabel *yearLabel;

@property (weak, nonatomic) UILabel *monthLabel;

@end

@implementation YSCalendarHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame]){
        
        [self setButton:frame];
        
        [self setDateLabel:frame];
        
    }
    return self;
}

#pragma mark - SET 
- (void)setYear:(NSInteger)year
{
    _year = year;
    self.yearLabel.text = [NSString stringWithFormat:@"%ld",year];
}

- (void)setMonth:(NSInteger)month
{
    _month = month;
    
    NSArray *array = @[@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December"];
    
    self.monthLabel.text = array[month - 1];
}

- (void)setButton:(CGRect)rect{
    
    CGFloat height = CGRectGetHeight(rect);
    CGFloat width = CGRectGetWidth(rect);
    
    CGFloat leftX = BUTTONSPACING;
    CGFloat leftY = (height - WEEKHEIGHT - BUTTONHEIGHT) * 0.5;
    CGRect leftRect = CGRectMake(leftX, leftY, BUTTONWIDTH, BUTTONHEIGHT);
    
    CGFloat rightX = width - BUTTONSPACING - BUTTONWIDTH;
    CGFloat rightY = leftY;
    CGRect rightRect = CGRectMake(rightX, rightY, BUTTONWIDTH, BUTTONHEIGHT);
    
    UIButton *leftButton = [[UIButton alloc] initWithFrame:leftRect];
    UIButton *rightButton = [[UIButton alloc] initWithFrame:rightRect];
    
    [leftButton setImage:[UIImage imageNamed:@"kal_left_arrow"] forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"kal_right_arrow"] forState:UIControlStateNormal];
    
    [self addSubview:leftButton];
    [self addSubview:rightButton];
    
    [leftButton addTarget:self action:@selector(leftClick:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton addTarget:self action:@selector(rightClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setDateLabel:(CGRect)rect
{
    CGFloat centerX = CGRectGetWidth(rect) * 0.5f;
    CGFloat centerY = (CGRectGetHeight(rect) - WEEKHEIGHT) * 0.5f;
    
    CGFloat yearX = centerX - DAYYEARSPACING * 0.5 - YEARWIDTH;
    CGFloat yearY = centerY - YEARHEIGHT * 0.5;
    CGRect yearRect = CGRectMake(yearX, yearY, YEARWIDTH, YEARHEIGHT);
    
    CGFloat monthX = centerX + DAYYEARSPACING * 0.5f;
    CGFloat monthY = yearY;
    CGRect monthRect = CGRectMake(monthX, monthY, MONTHWIDTH, MONTHHEIGHT);
    
    UILabel *yearL = [[UILabel alloc] initWithFrame:yearRect];
    UILabel *monthL = [[UILabel alloc] initWithFrame:monthRect];
    
    yearL.textColor = [UIColor blackColor];
    monthL.textColor = [UIColor blackColor];
    
    yearL.backgroundColor = [UIColor clearColor];
    monthL.backgroundColor = [UIColor clearColor];
    
    [self addSubview:yearL];
    [self addSubview:monthL];
    
    self.yearLabel = yearL;
    self.monthLabel = monthL;
}

- (void)leftClick:(UIButton *)sender
{
    if([self.delegate respondsToSelector:@selector(YSCalendarLeftButtonClick:)]){
        [self.delegate YSCalendarLeftButtonClick:sender];
    }
}

- (void)rightClick:(UIButton *)sender
{
    if([self.delegate respondsToSelector:@selector(YSCalendarRightButtonClick:)]){
        [self.delegate YSCalendarRightButtonClick:sender];
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    [self drawWeek:rect];
}

- (void)drawWeek:(CGRect)rect
{
    NSArray *array = @[@"Sun",@"Mon",@"Tue",@"Wed",@"Thu",@"Fri",@"Sat"];
    
    CGFloat height = CGRectGetHeight(rect);
    
    CGFloat perWidth = CGRectGetWidth(rect) / array.count;
    
    [array enumerateObjectsUsingBlock:^(NSString *  _Nonnull text, NSUInteger idx, BOOL * _Nonnull stop) {
        
        CGFloat textX = idx * perWidth + (perWidth - TEXTWIDTH) * 0.5;
        
        CGFloat textY = height - WEEKHEIGHT * 0.5 - TEXTHEIGHT * 0.5;
        
        CGRect textRect = CGRectMake(textX, textY, TEXTWIDTH, TEXTHEIGHT);
        
        NSDictionary *attributeDic = @{NSFontAttributeName:[UIFont systemFontOfSize:10.0f],NSForegroundColorAttributeName:[UIColor blackColor]};
        
        [text drawInRect:textRect withAttributes:attributeDic];
    }];
}

@end
