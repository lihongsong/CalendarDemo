//
//  YSCalendarFlowLayout.h
//  CalendarDemo
//
//  Created by 李泓松 on 16/7/19.
//  Copyright © 2016年 李泓松. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YSCalendarFlowLayout : UICollectionViewFlowLayout

+ (instancetype)YSCalendarFlowLayoutWithRect:(CGRect)rect;

@end
